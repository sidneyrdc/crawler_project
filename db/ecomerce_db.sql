
CREATE TABLE public.shop (
                shop_id INTEGER NOT NULL,
                shop_name VARCHAR,
                shop_description VARCHAR,
                shop_url VARCHAR,
                CONSTRAINT shop_pk PRIMARY KEY (shop_id)
);


CREATE TABLE public.brand (
                brand_id INTEGER NOT NULL,
                brand_name VARCHAR,
                brand_description VARCHAR,
                brand_logo VARCHAR,
                CONSTRAINT brand_pk PRIMARY KEY (brand_id)
);


CREATE TABLE public.category (
                category_id INTEGER NOT NULL,
                category_name VARCHAR,
                category_description VARCHAR,
                category_seeds VARCHAR,
                CONSTRAINT category_pk PRIMARY KEY (category_id)
);


CREATE TABLE public.product (
                product_id INTEGER NOT NULL,
                brand_id INTEGER NOT NULL,
                product_name VARCHAR,
                product_description VARCHAR,
                product_img_url VARCHAR,
                product_hqimg_url VARCHAR,
                CONSTRAINT product_pk PRIMARY KEY (product_id, brand_id)
);


CREATE TABLE public.product_shop (
                shop_id INTEGER NOT NULL,
                product_id INTEGER NOT NULL,
                brand_id INTEGER NOT NULL,
                product_shop_price REAL,
                product_shop_discount REAL,
                product_shop_url VARCHAR,
                product_shop_outofstock BOOLEAN DEFAULT False,
                product_shop_rate NUMERIC,
                CONSTRAINT product_shop_pk PRIMARY KEY (shop_id, product_id, brand_id)
);


CREATE TABLE public.product_category (
                category_id INTEGER NOT NULL,
                product_id INTEGER NOT NULL,
                brand_id INTEGER NOT NULL,
                CONSTRAINT product_category_pk PRIMARY KEY (category_id, product_id, brand_id)
);


ALTER TABLE public.product_shop ADD CONSTRAINT shop_product_shop_fk
FOREIGN KEY (shop_id)
REFERENCES public.shop (shop_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product ADD CONSTRAINT brand_product_fk
FOREIGN KEY (brand_id)
REFERENCES public.brand (brand_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product_category ADD CONSTRAINT category_product_category_fk
FOREIGN KEY (category_id)
REFERENCES public.category (category_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product_category ADD CONSTRAINT product_product_category_fk
FOREIGN KEY (product_id, brand_id)
REFERENCES public.product (product_id, brand_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product_shop ADD CONSTRAINT product_product_shop_fk
FOREIGN KEY (product_id, brand_id)
REFERENCES public.product (product_id, brand_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
