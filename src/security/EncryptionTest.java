package security;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public class EncryptionTest {
    public static void main(String[] args) throws Exception {
        byte[] keyBytes = "1234567890azertyuiopqsdf".getBytes("ASCII");
        DESedeKeySpec keySpec = new DESedeKeySpec(keyBytes);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
        SecretKey key = factory.generateSecret(keySpec);
        byte[] text = "Hello world".getBytes("ASCII");

        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(text);
        
        String crypt = "";
        
        for(byte b : encrypted) crypt +=b+";";
        
        System.out.println(crypt);
        
        
        cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decrypted = cipher.doFinal(encrypted);
        System.out.println(new String(encrypted));
        System.out.println(new String(decrypted, "ASCII"));
    }
}