package security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public class Cryptography {
	private byte[] keyBytes;
	private SecretKey key;
	private Cipher cipher;
	
	// Initial setup
	public Cryptography(String phrase) {
		try {
			keyBytes = phrase.getBytes("ASCII");
			DESedeKeySpec keySpec = new DESedeKeySpec(keyBytes);
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
	        key = factory.generateSecret(keySpec);
	        
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
	
	// Encrypt a input string
	public String encrypt(String input) {
		byte[] text;
		String out = "";
		
		try {
			text = input.getBytes("ASCII");
			cipher = Cipher.getInstance("DESede");
	        cipher.init(Cipher.ENCRYPT_MODE, key);
	        byte[] encrypted = cipher.doFinal(text);
	        
	        for(byte b : encrypted) out += b +""+ (char) genRandomNumber(0x61, 0x7A);
	        
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		
		return out;
	}
	
	// Decrypt a encrypted input string
	public String decrypt(String input) {
		byte[] decrypted = null;
		
		String[] inputc = input.split("[A-Za-z]");
		
		byte[] encrypted = new byte[inputc.length];
		
		for(int i = 0; i < inputc.length; i++) {
			encrypted[i] = (byte) Integer.parseInt(inputc[i]);
		}
		
		try {
			cipher = Cipher.getInstance("DESede");
			cipher.init(Cipher.DECRYPT_MODE, key);
	        decrypted = cipher.doFinal(encrypted);
	        
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		
		try {
			return new String(decrypted, "ASCII");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	// Generate a random number between a minimum and maximum values
	public int genRandomNumber(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}
	
	public static void main(String[] args) {
		Cryptography c = new Cryptography("strykermasteropoderoso1223");
		
		String k = c.encrypt("teste123");
		System.out.println(k);
		System.out.println(c.decrypt(k));;
	}
}
