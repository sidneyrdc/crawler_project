package structs;

public class Product {
	private String name;
	private String description;
	private String url;
	private String img_url;
	private String hqimg_url;
	private float price;
	private float discount;
	private float rate;
	private boolean outOfStock;
	private Category category;
	private Brand brand;
	private Shop shop;
	
	// Default constructor
	public Product() {
		name = "";
		description = "";
		url = "";
		img_url = "";
		hqimg_url = "";
		price = 0;
		discount = 0;
		rate = 0;
		outOfStock = false;
		category = new Category(null, null);
		brand = new Brand(null, null, null);
		shop = new Shop(null, null, null);
	}
	
	@Override
	public String toString() {		
		return "prod_name:" + name + "\n" +
			   "prod_desc:" + description + "\n" +
			   "prod_url:" + url + "\n" +
			   "prod_img:" + img_url + "\n" +
			   "prod_hqimg:" + hqimg_url + "\n" +
			   "prod_price:" + price + "\n" +
			   "prod_disco:" + discount + "\n" +
			   "prod_rate:" + rate + "\n" +
			   "prod_deal:" + outOfStock + "\n" +
			   "prod_category:" + category.getName() + "\n" +
			   "prod_brand:" + brand.getName() + "\n" +
			   "prod_shop:" + shop.getName() + "\n";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	
	public String getHqimg_url() {
		return hqimg_url;
	}

	public void setHqimg_url(String hqimg_url) {
		this.hqimg_url = hqimg_url;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public boolean isOutOfStock() {
		return outOfStock;
	}

	public void setOutOfStock(boolean deal) {
		this.outOfStock = deal;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
