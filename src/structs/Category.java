package structs;

public class Category {
	private String name;
	private String description;
	private String seeds;
	
	// Default constructor
	public Category(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	// Alternative constructor
	public Category(String name, String description, String seeds) {
		this.name = name;
		this.description = description;
		this.seeds = seeds;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getSeeds() {
		return seeds;
	}

	public void setSeeds(String seeds) {
		this.seeds = seeds;
	}
}
