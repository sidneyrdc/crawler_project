package gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CategoryGUI extends JDialog {
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField tfName;
	private JTextField tfDescription;
	private JTextArea taSeeds;
	private boolean wasSaved = false;
	
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
//		try {
//			CategoryGUI dialog = new CategoryGUI();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public CategoryGUI() {
		setUndecorated(true);
		setBackground(new Color(214, 217, 223));
		getContentPane().setBackground(new Color(214, 217, 223));
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Category Manager");
		setResizable(false);
		setBounds(100, 100, 600, 400);
		setSize(600, 378);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(214, 217, 223));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 99, 576, 226);
		contentPanel.add(scrollPane);
		
		taSeeds = new JTextArea();
		taSeeds.setLineWrap(true);
		scrollPane.setViewportView(taSeeds);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(12, 12, 576, 66);
		contentPanel.add(panel);
		panel.setLayout(null);
		
		JLabel lblCategoryName = new JLabel("Category Name:");
		lblCategoryName.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
		lblCategoryName.setBounds(12, 10, 120, 15);
		panel.add(lblCategoryName);
		
		tfName = new JTextField();
		tfName.setFont(new Font("Dialog", Font.PLAIN, 11));
		tfName.setBounds(12, 30, 212, 27);
		panel.add(tfName);
		tfName.setColumns(10);
		
		tfDescription = new JTextField();
		tfDescription.setFont(new Font("Dialog", Font.PLAIN, 11));
		tfDescription.setBounds(283, 30, 281, 27);
		panel.add(tfDescription);
		tfDescription.setColumns(10);
		
		JLabel lblCategoryDescription = new JLabel("Category Description:");
		lblCategoryDescription.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
		lblCategoryDescription.setBounds(283, 10, 197, 15);
		panel.add(lblCategoryDescription);
		
		JLabel lblSeeds = new JLabel("Seeds:");
		lblSeeds.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
		lblSeeds.setBounds(12, 83, 70, 15);
		contentPanel.add(lblSeeds);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfName.getText() == null || tfName.getText().equals("")) {
					JOptionPane.showMessageDialog(getContentPane(), "The category needs a name!", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					wasSaved = true;
					dispose();
				}
			}
		});
		btnSave.setBounds(471, 337, 117, 25);
		contentPanel.add(btnSave);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				wasSaved = false;
				dispose();
			}
		});
		btnCancel.setBounds(342, 337, 117, 25);
		contentPanel.add(btnCancel);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}

	public String getName() {
		return tfName.getText();
	}
	
	public void setName(String name) {
		tfName.setText(name);
	}

	public String getDescription() {
		return tfDescription.getText();
	}
	
	public void setDescription(String description) {
		tfDescription.setText(description);
	}

	public String getSeeds() {
		return taSeeds.getText();
	}
	
	public void setSeeds(String seeds) {
		taSeeds.setText(seeds);
	}

	public boolean isWasSaved() {
		return wasSaved;
	}
}
