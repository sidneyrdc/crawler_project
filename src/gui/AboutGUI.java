package gui;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import javax.swing.UIManager;

import java.awt.Label;
import java.awt.Font;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AboutGUI extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			AboutGUI dialog = new AboutGUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AboutGUI() {
		setModal(true);
		setResizable(false);
		setUndecorated(true);
		getContentPane().setBackground(new Color(214, 217, 223));
		setBounds(100, 100, 451, 312);
		getContentPane().setLayout(null);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setBackground(Color.LIGHT_GRAY);
		mainPanel.setBounds(12, 12, 427, 248);
		getContentPane().add(mainPanel);
		mainPanel.setLayout(null);
		
		Label lblName = new Label("Product Extractor 1.0");
		lblName.setEnabled(false);
		lblName.setAlignment(Label.CENTER);
		lblName.setFont(new Font("DejaVu Sans", Font.BOLD, 27));
		lblName.setBounds(10, 125, 407, 44);
		mainPanel.add(lblName);
		
		JLabel lblImage = new JLabel();
		lblImage.setBounds(112, 6, 200, 113);
		mainPanel.add(lblImage);
		lblImage.setIcon(new ImageIcon(AboutGUI.class.getResource("/img/background-webcrawler.png")));
		
		JLabel lblCopyrightSidney = new JLabel("Copyright 2014-2015 Sidney R. Carvalho");
		lblCopyrightSidney.setBounds(58, 175, 322, 15);
		mainPanel.add(lblCopyrightSidney);
		
		JLabel lblThisIsA = new JLabel("This is a webcrawler program to extract products");
		lblThisIsA.setBounds(58, 197, 322, 15);
		mainPanel.add(lblThisIsA);
		
		JLabel lblSomeDigitalShop = new JLabel("from digital shops in the web.");
		lblSomeDigitalShop.setBounds(58, 219, 322, 15);
		mainPanel.add(lblSomeDigitalShop);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnOk.setBounds(339, 272, 100, 27);
		getContentPane().add(btnOk);
	}
}
