package gui;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.DefaultComboBoxModel;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class DataBaseGUI extends JDialog {
	private static final long serialVersionUID = 1L;
	private JTextField tfConName;
	private JTextField tfHostname;
	private JTextField tfPort;
	private JTextField tfDatabase;
	private JTextField tfUser;
	private JPasswordField pwfPassword;
	private JComboBox cmbDatabaseType;
	private boolean wasSaved = false;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
//		try {
//			DataBaseGUI dialog = new DataBaseGUI();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public DataBaseGUI() {
		setUndecorated(true);
		setTitle("Database Settings");
		setResizable(false);
		setBackground(new Color(223, 223, 223));
		setModal(true);
		setAutoRequestFocus(false);
		setBounds(100, 100, 450, 232);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(6, 6, 438, 178);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		tfConName = new JTextField();
		tfConName.setBounds(6, 23, 202, 27);
		panel.add(tfConName);
		tfConName.setColumns(10);
		
		JLabel lblConnectionName = new JLabel("Connection Name:");
		lblConnectionName.setBounds(6, 6, 147, 15);
		panel.add(lblConnectionName);
		
		cmbDatabaseType = new JComboBox();
		cmbDatabaseType.setModel(new DefaultComboBoxModel(new String[] {"PostgreSQL"}));
		cmbDatabaseType.setBounds(230, 24, 202, 25);
		panel.add(cmbDatabaseType);
		
		JLabel lblDatabaseType = new JLabel("Database Type:");
		lblDatabaseType.setBounds(233, 6, 111, 15);
		panel.add(lblDatabaseType);
		
		tfHostname = new JTextField();
		tfHostname.setBounds(6, 81, 202, 27);
		panel.add(tfHostname);
		tfHostname.setColumns(10);
		
		JLabel lblHostname = new JLabel("Hostname:");
		lblHostname.setBounds(6, 66, 82, 15);
		panel.add(lblHostname);
		
		tfPort = new JTextField();
		tfPort.setBounds(230, 81, 65, 27);
		panel.add(tfPort);
		tfPort.setColumns(10);
		
		if(cmbDatabaseType.getSelectedItem().toString().equals("PostgreSQL")) tfPort.setText("5432");
		
		tfDatabase = new JTextField();
		tfDatabase.setBounds(307, 81, 125, 27);
		panel.add(tfDatabase);
		tfDatabase.setColumns(10);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(230, 66, 39, 15);
		panel.add(lblPort);
		
		JLabel lblDatabase = new JLabel("Database:");
		lblDatabase.setBounds(307, 66, 82, 15);
		panel.add(lblDatabase);
		
		tfUser = new JTextField();
		tfUser.setBounds(6, 143, 202, 27);
		panel.add(tfUser);
		tfUser.setColumns(10);
		
		pwfPassword = new JPasswordField();
		pwfPassword.setBounds(230, 143, 202, 27);
		panel.add(pwfPassword);
		
		JLabel lblUser = new JLabel("User:");
		lblUser.setBounds(6, 129, 60, 15);
		panel.add(lblUser);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(230, 129, 72, 15);
		panel.add(lblPassword);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(tfConName.getText() == null || tfConName.getText().equals("")) {
					JOptionPane.showMessageDialog(getContentPane(), "The configuration needs a name!", "Error", JOptionPane.ERROR_MESSAGE);
				} else if(tfDatabase.getText() == null || tfDatabase.getText().equals("")) {
					JOptionPane.showMessageDialog(getContentPane(), "The database cannot be null!", "Error", JOptionPane.ERROR_MESSAGE);
				} else if(tfHostname.getText() == null || tfHostname.getText().equals("")) {
					JOptionPane.showMessageDialog(getContentPane(), "The hostname must be defined!", "Error", JOptionPane.ERROR_MESSAGE);
				} else if(tfPort.getText() == null || tfPort.getText().equals("")) {
					JOptionPane.showMessageDialog(getContentPane(), "The port must be defined!", "Error", JOptionPane.ERROR_MESSAGE);
				} else if(tfUser.getText() == null || tfUser.getText().equals("")) {
					JOptionPane.showMessageDialog(getContentPane(), "The user cannot be blank!", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					wasSaved = true;
					dispose();
				}
			}
		});
		btnSave.setBounds(344, 196, 100, 27);
		getContentPane().add(btnSave);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				wasSaved = false;
				dispose();
			}
		});
		btnCancel.setBounds(232, 196, 100, 27);
		getContentPane().add(btnCancel);
	}

	public String getConName() {
		return tfConName.getText();
	}
	
	public void setConName(String name) {
		tfConName.setText(name);
	}

	public String getHostname() {
		return tfHostname.getText();
	}
	
	public void setHostname(String name) {
		tfHostname.setText(name);
	}

	public String getPort() {
		return tfPort.getText();
	}
	
	public void setPort(String port) {
		tfPort.setText(port);
	}

	public String getDatabase() {
		return tfDatabase.getText();
	}
	
	public void setDatabase(String database) {
		tfDatabase.setText(database);
	}

	public String getUser() {
		return tfUser.getText();
	}
	
	public void setUser(String user) {
		tfUser.setText(user);
	}

	public String getPassword() {
		String password = "";
		
		for(char c : pwfPassword.getPassword()) password += c;
		
		return password;
	}
	
	public void setPassword(String password) {
		pwfPassword.setText(password);
	}

	public boolean isWasSaved() {
		return wasSaved;
	}
	
	public String getDatabaseType() {
		return cmbDatabaseType.getSelectedItem().toString();
	}
	
	public void setDatabaseType(String type) {
		cmbDatabaseType.setSelectedItem(type);
	}
}
