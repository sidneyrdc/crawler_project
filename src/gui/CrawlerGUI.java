package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JPanel;

import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

import security.Cryptography;
import structs.Category;
import structs.Product;
import utils.Colors;
import utils.MessageConsole;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;

import crawler.Crawler;
import database.access.DatabaseManager;
import database.dbconnection.JDBCConnection;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Toolkit;

import javax.swing.SwingConstants;


public class CrawlerGUI {
	private JFrame frmProductExtractor;
	private JTable tbCategory;
	private CategoryGUI categoryGui;
	private DataBaseGUI databaseGui;
	private Cryptography security;
	private JDBCConnection dbConnection;
	private int cycleTime;
	private Extractor extractorThread;
	private JFormattedTextField ftfWait;
	private boolean isStoppable;
	
	private final String CONFIG_FILE = ".database";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new CrawlerGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CrawlerGUI() {
		// Initialize graphic interface
		initialize();
		
		// Cryptography key
		security = new Cryptography("St7ykerMaster0poderosoDo5niver5o2TodosdeVenA7oelha0se");
		
		// Open database connection
		openDatabase();
		
		// Update table view
		updateTable();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("serial")
	private void initialize() {
		frmProductExtractor = new JFrame();
		frmProductExtractor.setIconImage(Toolkit.getDefaultToolkit().getImage(CrawlerGUI.class.getResource("/img/icon-webcrawler.png")));
		frmProductExtractor.setTitle("Product Extractor");
		frmProductExtractor.setResizable(false);
		frmProductExtractor.setBounds(100, 100, 600, 512);
		frmProductExtractor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmProductExtractor.getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 600, 21);
		frmProductExtractor.getContentPane().add(menuBar);
		
		JMenu mnCategory = new JMenu("Categories");
		menuBar.add(mnCategory);
		
		final JMenuItem mntmAddCategory = new JMenuItem("Add");
		mntmAddCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openCategoryGUI();
			}
		});
		mnCategory.add(mntmAddCategory);
		
		final JMenuItem mntmDelCategory = new JMenuItem("Delete");
		mntmDelCategory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				delCategory();
			}
		});
		mntmDelCategory.setEnabled(false);
		mnCategory.add(mntmDelCategory);
		
		final JMenuItem mntmManageCategory = new JMenuItem("Manage");
		mntmManageCategory.setEnabled(false);
		mntmManageCategory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				editCategory();
			}
		});;
		mnCategory.add(mntmManageCategory);
		
		JMenu mnPreferences = new JMenu("Preferences");
		menuBar.add(mnPreferences);
		
		final JMenuItem mntmDatabase = new JMenuItem("Database");
		mntmDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openDatabaseGUI();
				openDatabase();
			}
		});
		mnPreferences.add(mntmDatabase);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AboutGUI about = new AboutGUI();
				about.setLocationRelativeTo(frmProductExtractor);
				about.setVisible(true);
			}
		});
		mnHelp.add(mntmAbout);
		
		JScrollPane spTable = new JScrollPane();
		spTable.setBounds(12, 83, 576, 104);
		frmProductExtractor.getContentPane().add(spTable);
		
		tbCategory = new JTable();
		tbCategory.setToolTipText("");
		tbCategory.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Extract?", "Category", "# of Seeds", "Seeds", "Description"
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Boolean.class, String.class, Integer.class, Object.class, String.class
			};
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				true, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tbCategory.getColumnModel().getColumn(0).setResizable(false);
		tbCategory.getColumnModel().getColumn(0).setPreferredWidth(65);
		tbCategory.getColumnModel().getColumn(0).setMaxWidth(65);
		tbCategory.getColumnModel().getColumn(1).setResizable(false);
		tbCategory.getColumnModel().getColumn(1).setPreferredWidth(186);
		tbCategory.getColumnModel().getColumn(2).setResizable(false);
		tbCategory.getColumnModel().getColumn(2).setPreferredWidth(100);
		tbCategory.getColumnModel().getColumn(2).setMaxWidth(100);
		tbCategory.getColumnModel().getColumn(3).setResizable(false);
		tbCategory.getColumnModel().getColumn(4).setResizable(false);
		
		tbCategory.getColumnModel().getColumn(3).setMinWidth(0);
		tbCategory.getColumnModel().getColumn(3).setMaxWidth(0);
		tbCategory.getColumnModel().getColumn(4).setMinWidth(0);
		tbCategory.getColumnModel().getColumn(4).setMaxWidth(0);
		
		tbCategory.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		spTable.setViewportView(tbCategory);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(12, 27, 576, 51);
		frmProductExtractor.getContentPane().add(panel);
		panel.setLayout(null);
		
		final JButton btAdd = new JButton("");
		btAdd.setBounds(12, 12, 27, 28);
		btAdd.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/add.png")));
		panel.add(btAdd);
		btAdd.setToolTipText("Add Category");
		
		final JButton btDel = new JButton("");
		btDel.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/del.png")));
		btDel.setToolTipText("Delete Category");
		btDel.setBounds(51, 12, 27, 28);
		btDel.setEnabled(false);
		panel.add(btDel);
		
		final JButton btStart = new JButton("");
		btStart.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/start.png")));
		btStart.setToolTipText("Start Extraction");
		btStart.setBounds(275, 12, 27, 28);
		panel.add(btStart);
		
		final JButton btStop = new JButton("");
		btStop.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/stop.png")));
		btStop.setToolTipText("Stop Extraction");
		btStop.setBounds(314, 12, 27, 28);
		btStop.setEnabled(false);
		panel.add(btStop);
		
		final JSpinner spTimer = new JSpinner();
		spTimer.setFont(new Font("DejaVu Sans", Font.PLAIN, 11));
		spTimer.setModel(new SpinnerNumberModel(1, 1, 999, 1));
		spTimer.setToolTipText("Number of hours between each cycle");
		spTimer.setBounds(210, 12, 55, 28);
		panel.add(spTimer);
		
		ftfWait = new JFormattedTextField();
		ftfWait.setHorizontalAlignment(SwingConstants.CENTER);
		ftfWait.setForeground(Color.RED);
		ftfWait.setFont(new Font("FreeMono", Font.BOLD, 14));
		ftfWait.setToolTipText("Time left until the next cycle");
		ftfWait.setEditable(false);
		ftfWait.setBounds(474, 12, 90, 28);
		panel.add(ftfWait);
		ftfWait.setText("--:--");
		
		JLabel lblWaiting = new JLabel("Waiting:");
		lblWaiting.setBounds(419, 18, 60, 15);
		panel.add(lblWaiting);
		
		JLabel lblTime = new JLabel("Timer (h):");
		lblTime.setBounds(141, 18, 68, 15);
		panel.add(lblTime);
		
		JScrollPane spConsole = new JScrollPane();
		spConsole.setBounds(12, 213, 576, 265);
		frmProductExtractor.getContentPane().add(spConsole);
		
		final JTextArea taConsole = new JTextArea();
		taConsole.setEditable(false);
		taConsole.setFont(new Font("Monospaced", Font.PLAIN, 10));
		taConsole.setLineWrap(true);
		spConsole.setViewportView(taConsole);
		
		// Console emulator
		MessageConsole mc = new MessageConsole(taConsole);
		
		// Redirect error and output
		mc.redirectOut();
		mc.redirectErr(Color.RED, null);
		
//		TextAreaOutputStream taOutputStream = new TextAreaOutputStream(taConsole, "");
//		// Set default output to textarea
//		System.setOut(new PrintStream(taOutputStream));
		
		JLabel lblConsole = new JLabel("Console:");
		lblConsole.setBounds(12, 197, 70, 15);
		frmProductExtractor.getContentPane().add(lblConsole);
		
		btAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openCategoryGUI();
			}
		});
		
		btDel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				delCategory();
			}
		});
		
		btStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Category> categoryList = new ArrayList<>();
				
				// Get cycle time
				cycleTime = (int) spTimer.getValue();
				
				// Clear console
				taConsole.setText("");
				
				// Get table model
				TableModel model = tbCategory.getModel();
				
				// Get all category information
				for(int i = 0; i < model.getRowCount(); i++) {
					if((boolean) model.getValueAt(i, 0) == true) {
						Category c = new Category(model.getValueAt(i, 1).toString(), "", model.getValueAt(i, 3).toString());

						categoryList.add(c);
					}
				}
				
				if(categoryList.size() > 0) {
					// Unlock thread execution
					isStoppable = true;
					
					// Initialize extractor thread
					extractorThread = new Extractor(categoryList);
					extractorThread.start();
					
					// Disable buttons
					btStart.setEnabled(false);
					btAdd.setEnabled(false);
					btDel.setEnabled(false);
					tbCategory.setEnabled(false);
					spTimer.setEnabled(false);
					mntmAddCategory.setEnabled(false);
					mntmDelCategory.setEnabled(false);
					mntmManageCategory.setEnabled(false);
					mntmDatabase.setEnabled(false);
					btStop.setEnabled(true);
				} else {
					JOptionPane.showMessageDialog(frmProductExtractor, "You must select at least one category to extract!", "Message", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
		btStop.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				if(isStoppable == true) {
					// Dialog message
					int response = JOptionPane.showConfirmDialog(frmProductExtractor, "Do you want stop the extraction?", "Stop Extraction", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					// Check the response
					if(response == JOptionPane.YES_OPTION) {
						extractorThread.stop();
						ftfWait.setText("--:--");

						// Enable buttons
						btStart.setEnabled(true);
						btAdd.setEnabled(true);
						tbCategory.setEnabled(true);
						spTimer.setEnabled(true);
						mntmAddCategory.setEnabled(true);
						mntmDatabase.setEnabled(true);
						btStop.setEnabled(false);
					}
				} else {
					JOptionPane.showMessageDialog(frmProductExtractor, "You can't stop the extraction during database insertion!", "Warning", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		btAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btAdd.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/add.png")));
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				btAdd.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/add1.png")));
			}
		});
		
		btDel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btDel.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/del.png")));
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				btDel.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/del1.png")));
			}
		});
		
		btStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btStart.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/start.png")));
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				btStart.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/start1.png")));
			}
		});
		
		btStop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btStop.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/stop.png")));
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				btStop.setIcon(new ImageIcon(CrawlerGUI.class.getResource("/img/stop1.png")));
			}
		});
		
		tbCategory.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(tbCategory.isEnabled()) {
					btDel.setEnabled(true);
					mntmDelCategory.setEnabled(true);
					mntmManageCategory.setEnabled(true);
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if(e.getClickCount() == 2 && tbCategory.isEnabled()) {
					editCategory();
				}
			}
		});
		
		frmProductExtractor.setVisible(true);
	}
	
	// Delete a selected category on table view
	private void delCategory() {
		// Get selected row
		int selRow = tbCategory.getSelectedRow();
		
		if(selRow >= 0) {
			// Dialog message
			int response = JOptionPane.showConfirmDialog(frmProductExtractor, "Do you want delete this category?", "Deletion Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			// Verify the answer
			if(response == JOptionPane.YES_OPTION) {
				// New database manager instance
				DatabaseManager dbm = new DatabaseManager(dbConnection);

				// Get table model
				TableModel model = tbCategory.getModel();

				// Loads values from the table
				Category c = new Category(model.getValueAt(selRow, 1).toString(), 
										  model.getValueAt(selRow, 4).toString(),
										  model.getValueAt(selRow, 3).toString());

				// Delete the category from database
				dbm.delCategory(c);

				// Update table view
				updateTable();
			}
		}
	}
	
	// Edit a selected category on table view
	private void editCategory() {
		// Get selected row
		int selRow = tbCategory.getSelectedRow();
		
		if(selRow >= 0) {
			// Get table model
			TableModel model = tbCategory.getModel();

			// Loads values from the table
			Category c = new Category(model.getValueAt(selRow, 1).toString(), 
									  model.getValueAt(selRow, 4).toString(),
									  model.getValueAt(selRow, 3).toString());

			// Open category gui
			openCategoryGUI(c);

			// Update table view
			updateTable();
		}
	}
	
	// Open the category visual interface
	private void openCategoryGUI(Category... input) {
		categoryGui = new CategoryGUI();
		categoryGui.setLocationRelativeTo(frmProductExtractor);
		
		// Load values from a existing category
		if(input.length > 0) {
			categoryGui.setName(input[0].getName());
			categoryGui.setDescription(input[0].getDescription());
			categoryGui.setSeeds(input[0].getSeeds());
		}
		
		categoryGui.setVisible(true);
		
		// If save button as pressed
		if(categoryGui.isWasSaved()) {
			// Create new category with the values from the gui
			Category category = new Category(categoryGui.getName(), categoryGui.getDescription(), categoryGui.getSeeds());
			
			// Initialize a new storage action on database
			DatabaseManager dbStore = new DatabaseManager(dbConnection);
			
			// Store the new category with its seeds
			if(input.length > 0) dbStore.storeSeeds(input[0], category);
			else dbStore.storeSeeds(category);
			
			// Update table view
			updateTable();
		}
	}
	
	// Open the database visual interface
	private boolean openDatabaseGUI() {
		String dbConfig = loadFile(CONFIG_FILE);
		
		databaseGui = new DataBaseGUI();
		databaseGui.setLocationRelativeTo(frmProductExtractor);
		
		// Get values from configuration file
		if(dbConfig != null && !dbConfig.equals("")) {
			databaseGui.setConName(dbConfig.split("Connection_name:")[1].split(";")[0]);
			databaseGui.setDatabaseType(dbConfig.split("Database_type:")[1].split(";")[0]);
			databaseGui.setHostname(dbConfig.split("Hostname:")[1].split(";")[0]);
			databaseGui.setPort(dbConfig.split("Port:")[1].split(";")[0]);
			databaseGui.setDatabase(dbConfig.split("Database:")[1].split(";")[0]);
			databaseGui.setUser(dbConfig.split("Username:")[1].split(";")[0]);
			databaseGui.setPassword(security.decrypt(dbConfig.split("Password:")[1].split(";")[0]));
		}
	
		databaseGui.setVisible(true);
		
		// Verify if the save button was pressed
		if(databaseGui.isWasSaved()) {
			String data = "Connection_name:"+databaseGui.getConName()+";\n"+
						  "Database_type:"+databaseGui.getDatabaseType()+";\n"+
						  "Hostname:"+databaseGui.getHostname()+";\n"+
						  "Port:"+databaseGui.getPort()+";\n"+
						  "Database:"+databaseGui.getDatabase()+";\n"+
						  "Username:"+databaseGui.getUser()+";\n"+
						  "Password:"+security.encrypt(databaseGui.getPassword())+";";
			
			// Update configuration file
			writeFile(data, CONFIG_FILE);
		}
		
		return databaseGui.isWasSaved();
	}
	
	// Open database connection
	private void openDatabase() {
		String exception = null;
		dbConnection = new JDBCConnection();
		
		String dbConfig = loadFile(CONFIG_FILE);
		
		if(dbConfig == null || dbConfig.equals("")) {
			do {
				JOptionPane.showMessageDialog(frmProductExtractor, "You must configure a database before anything!", "Information", JOptionPane.INFORMATION_MESSAGE);
				openDatabaseGUI();
			} while(!databaseGui.isWasSaved());
			
			dbConfig = loadFile(CONFIG_FILE);
		} 

		do {
			try {
				exception = null;
				dbConfig = loadFile(CONFIG_FILE);
				dbConnection.StartConnection(dbConfig.split("Username:")[1].split(";")[0],
											 security.decrypt(dbConfig.split("Password:")[1].split(";")[0]), 
											 dbConfig.split("Hostname:")[1].split(";")[0],
											 dbConfig.split("Database:")[1].split(";")[0],
											 dbConfig.split("Port:")[1].split(";")[0]);
				
				if(dbConnection.tablesNumber() == 0) dbConnection.createDB();
			} catch (SQLException e) {
				exception = e.getMessage();
				JOptionPane.showMessageDialog(frmProductExtractor, exception, "Error", JOptionPane.ERROR_MESSAGE);
				
				if(!openDatabaseGUI()) System.exit(1);
			}
		} while(exception != null);
		
		// Update table view
		updateTable();
	}
	
	// Update table view
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateTable() {
		// Default table model assigned to it
		DefaultTableModel model = (DefaultTableModel) tbCategory.getModel();
		
		// Remove all existing rows in the table
		while(model.getRowCount() > 0) model.removeRow(0);
		
		// Database manager connection
		DatabaseManager dbm = new DatabaseManager(dbConnection);
		
		// Get all data stored in category table on database
		ArrayList<Category> categoryList = dbm.pullCategory();
		
		// Fill all rows of the table
		for(Category c : categoryList) {
			Vector row = new Vector();
			
			row.add(false);
			row.add(c.getName());
			row.add(c.getSeeds().split("\n").length);
			row.add(c.getSeeds());
			row.add(c.getDescription());
			
			model.addRow(row);
		}
	}
	
	// Write data in a file
	private void writeFile(String data, String filename) {
		File file = new File(filename);

		PrintWriter output;
		try {
			output = new PrintWriter(new FileWriter(file, false));
			output.write(data);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Load data from a file
	private String loadFile(String filename) {
		String content = null;
		
		File file = new File(filename);
		
		if(file.exists()) {
			try {
				FileReader reader = new FileReader(file);
				char[] chars = new char[(int) file.length()];
				reader.read(chars);
				content = new String(chars);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return content;
	}
	
	// Extractor products thread
	class Extractor extends Thread{
		private String[] seeds;
		private ArrayList<Category> categoryList;
		private ArrayList<Product> productList;
		private Crawler crawler;
		
		// Default constructor
		public Extractor(ArrayList<Category> categoryList) {
			this.categoryList = categoryList;
		}
		
		// Default runner
	    public void run(){
			crawler = new Crawler();
			DatabaseManager store = new DatabaseManager(dbConnection);
			
			int hour = 0;
			int minute = 0;
			
			long startTime = 0;
			long endTime = 0;
	    	
	    	// Extractor cycle
	    	while(true) {
	    		productList = new ArrayList<>();
	    		
	    		System.out.println("###############################################################################");
				System.out.println(Colors.ANSI_GREEN+"INFO:Extracting data on thread: "+Thread.currentThread().getName()+Colors.ANSI_RESET);
				ftfWait.setText("--:--");
				
				// t0
				startTime = System.currentTimeMillis();
	    		
				// Extract products to all categories
	    		for(Category c : categoryList) {
	    			seeds = c.getSeeds().split("\n");
	    			
	    			productList.addAll(crawler.extractProducts(seeds, c.getName()));
	    		}
	    		
	    		// MUTEX lock thread execution
	    		isStoppable = false;
	    		
	    		// Store products on database
	    		store.storeProducts(productList);
	    		
	    		// MUTEX unlock thread execution (can be stopped)
	    		isStoppable = true;
	    		
	    		// tf
	    		endTime = System.currentTimeMillis();
	    		
	    		System.out.println("###############################################################################");
	    		System.out.println(Colors.ANSI_GREEN+"INFO:"+productList.size()+" products extracted in "+((endTime - startTime) / 1000.0)+" s"+Colors.ANSI_RESET);
	    		
	    		// Timer
	    		try {
	    			hour = cycleTime;
	    			minute = 0;

	    			for(int i = 0; i < cycleTime*60; i++) {
	    				ftfWait.setText(String.format("%02d", hour)+":"+String.format("%02d", minute));
	    				
	    				if(minute == 0) {
	    					hour--;
	    					minute = 60;
	    				}
	    				
	    				sleep(60000);
	    				minute--;
	    			}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	    	}
	    }
	}
}


