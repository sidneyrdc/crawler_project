package database.access;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import structs.Category;
import structs.Product;
import utils.Colors;
import database.dbconnection.JDBCConnection;

public class DatabaseManager {
	private JDBCConnection connection;
	
	// The constructor
	public DatabaseManager(final String db, final String url, final String user, final String passwd, final String port) {
		connection = new JDBCConnection();
		
		try {
			// Initialize the connection
			connection.StartConnection(user, passwd, url, db, port);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Alternative constructor
	public DatabaseManager(JDBCConnection connection) {
		this.connection = connection;
	}
	
	// Delete a category from database
	public void delCategory(Category category) {
		// Remove product_category reference
		String query = "DELETE FROM product_category WHERE category_id IN "
					 + "(SELECT category_id FROM category WHERE LOWER(category_name) = LOWER('"+category.getName()+"') AND "
			 		 + "LOWER(category_description) = LOWER('"+category.getDescription()+"') AND "
	 		 		 + "LOWER(category_seeds) = LOWER('"+category.getSeeds()+"'))";
		connection.QueryAll(query);
		
		// Remove category 
		query = "DELETE FROM category WHERE LOWER(category_name) = LOWER('"+category.getName()+"') AND "
			 		 + "LOWER(category_description) = LOWER('"+category.getDescription()+"') AND "
	 		 		 + "LOWER(category_seeds) = LOWER('"+category.getSeeds()+"')";
		connection.QueryAll(query);
	}
	
	// Get categories from database
	public ArrayList<Category> pullCategory(String... condition) {
		ArrayList<Category> categoryList = new ArrayList<>();
		ResultSet r;
		
		// Verify the condition argument
		if(condition.length > 0) {
			r = connection.StartQuery(condition[0], "category");
		} else {
			r = connection.QueryAll("SELECT * FROM category");
		}
		
		// Extract data and save on category list
		try {
			while(r.next()) {
				Category c = new Category(r.getString("category_name"), 
										  r.getString("category_description"), 
										  r.getString("category_seeds"));
				
				categoryList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return categoryList;
	}
	
	// Store the seeds on database
	public void storeSeeds(Category category_t0, Category... category_t1) {
		String query = "";
		
		int idCategory = checkDb("LOWER(category_name) = LOWER('"+category_t0.getName()+"')", "category");
		
		System.out.println("-------------------------------------------------------------------------------");
		
		// If category does not exist in the database
		if(idCategory == -1) {
			System.out.println("INFO:storing category: "+Colors.ANSI_BLUE+category_t0.getName()+Colors.ANSI_RESET);
			
			idCategory = getLastID("category", "category_id") + 1;
			
			query = idCategory+",'"+category_t0.getName()+"','"+
			category_t0.getDescription()+"','"+category_t0.getSeeds()+"'";
			
			connection.Insert("category", query);
			
		// If category exist
		} else if(category_t1.length > 0) {
			System.out.println("INFO:updating category: "+Colors.ANSI_BLUE+category_t0.getName()+Colors.ANSI_RESET);
			
			query = "category_name = '"+category_t1[0].getName()+"',"+
					"category_description = '"+category_t1[0].getDescription()+"',"+
					"category_seeds = '"+category_t1[0].getSeeds()+"'";
			
			String condition = "category_id = '"+idCategory+"'";
			
			connection.Update("category", query, condition);
		}
	}
	
	// Store the products on database
	public void storeProducts(ArrayList<Product> products) {
		int idProduct = 0, idBrand = 0, idShop = 0, idCategory = 0;
		
		// Counters
		int updatedProduct = 0;
		int storedProduct = 0;
		
		// Get last id to base tables
		int lastIdProduct = getLastID("product", "product_id");
		int lastIdBrand = getLastID("brand", "brand_id");
		int lastIdShop = getLastID("shop", "shop_id");
		int lastIdCategory = getLastID("category", "category_id");
		
		System.out.println("-------------------------------------------------------------------------------");
		
		for(Product p : products) {
			String query = "";
			
			// Verify the presence of brand, shop, category and product in the database
			idBrand = checkDb("LOWER(brand_name) = LOWER('"+p.getBrand().getName()+"')", "brand");
			idShop = checkDb("LOWER(shop_name) = LOWER('"+p.getShop().getName()+"')", "shop");
			idCategory = checkDb("LOWER(category_name) = LOWER('"+p.getCategory().getName()+"')", "category");
			idProduct = checkDb("LOWER(product_name) = LOWER('"+p.getName()+"') AND LOWER(brand_name) = LOWER('"+p.getBrand().getName()+"')", "product, brand");
			
			// If brand does not exist in the database
			if(idBrand == -1) {
				System.out.println("INFO:storing brand: "+Colors.ANSI_PURPLE+p.getBrand().getName()+Colors.ANSI_RESET);
				
				idBrand = (lastIdBrand++) + 1;
				
				query = idBrand+",'"+ 
						p.getBrand().getName()+"','"+ 
						p.getBrand().getDescription()+"','"+
						p.getBrand().getLogo()+"'";
				connection.Insert("brand", query);
				
			// If brand exist in the database
			} else {
				query = "brand_id = "+idBrand+" AND brand_logo = ''";
				
				connection.Update("brand", "brand_logo = '"+p.getBrand().getLogo()+"'", query);
			}
			
			// If shop does not exist in the database
			if(idShop == -1) {
				System.out.println("INFO:storing shop: "+Colors.ANSI_YELLOW+p.getShop().getName()+Colors.ANSI_RESET);
				
				idShop = (lastIdShop++) + 1;
				
				query = idShop+",'"+ 
						p.getShop().getName()+"','"+ 
						p.getShop().getDescription()+"','"+ 
						p.getShop().getUrl()+"'";
				connection.Insert("shop", query);
				
			// Update if shop exist in the database
			} else {
				query = "shop_description = '"+p.getShop().getDescription() +
						"',shop_url = '"+p.getShop().getUrl()+"'";
				connection.Update("shop", query, "shop_id = "+idShop);
			}
			
			// If product does not exist in the database
			if(idProduct == -1) {
				System.out.println("INFO:storing product: "+Colors.ANSI_CYAN+p.getName()+Colors.ANSI_RESET);
				
				idProduct = (lastIdProduct++) + 1;
				
				query = idProduct+","+idBrand+",'"+p.getName()+"','"+
						p.getDescription()+"','"+p.getImg_url()+"','"+p.getHqimg_url()+"'";
				connection.Insert("product", query);
				
				query = idShop+","+idProduct+","+idBrand+","+p.getPrice()+","+
						p.getDiscount()+",'"+p.getUrl()+"',"+p.isOutOfStock()+","+
						p.getRate();
				connection.Insert("product_shop", query);
				
				storedProduct++;
				
			// If product exist in the database
			} else {
				query = "product_shop.product_id = product.product_id AND "+
						"product_shop.brand_id = product.brand_id AND "+
						"product_shop.shop_id = shop.shop_id AND "+
						"LOWER(product.product_name) = LOWER('"+p.getName()+"') AND "+
						"LOWER(shop.shop_name) = LOWER('"+p.getShop().getName()+"') AND "+
						"LOWER(brand.brand_name) = LOWER('"+p.getBrand().getName()+"')";
				
				int idProdShop = checkDb(query, "product,shop,brand,product_shop");
				
				// If product-shop already in database
				if(idProdShop != -1) {
					System.out.println("INFO:updating product price: "+p.getName());
					
					query = "product_shop_price = "+p.getPrice()+","+
							"product_shop_discount = "+p.getDiscount()+","+
							"product_shop_url = '"+p.getUrl()+"',"+
							"product_shop_outofstock = "+p.isOutOfStock()+","+
							"product_shop_rate = "+p.getRate();
					
					String condition = "product_shop.shop_id = "+idShop+" AND "+
									   "product_shop.brand_id = "+idBrand+" AND "+
									   "product_shop.product_id = "+idProduct;
					
					connection.Update("product_shop", query, condition);
					
					updatedProduct++;
				
				// If product-shop is not in database
				} else {
					System.out.println("INFO:storing product-shop: "+Colors.ANSI_RED+p.getName()+Colors.ANSI_RESET);
					
					query = idShop+","+idProduct+","+idBrand+","+p.getPrice()+","+
							p.getDiscount()+",'"+p.getUrl()+"',"+p.isOutOfStock()+","+
							p.getRate();
					connection.Insert("product_shop", query);
				}
			}
			
			// If category does not exist in the database
			if(idCategory == -1) {
				System.out.println("INFO:storing category: "+Colors.ANSI_BLUE+p.getCategory().getName()+Colors.ANSI_RESET);
				
				idCategory = (lastIdCategory++) + 1;
				
				query = idCategory+",'"+p.getCategory().getName()+"','"+
				p.getCategory().getDescription()+"',''";
				connection.Insert("category", query);
				
				query = idCategory+","+checkDb("LOWER(product_name) = LOWER('"+p.getName()+"') AND LOWER(brand_name) = ('"+
						p.getBrand().getName()+"')", "product, brand")+
						","+checkDb("LOWER(brand_name) = LOWER('"+p.getBrand().getName()+"')", "brand");
				connection.Insert("product_category", query);
				
			// If category exist in the database
			} else {
				query = "product_category.product_id = product.product_id AND " +
						"product_category.brand_id = product.brand_id AND " + 
						"product_category.category_id = category.category_id AND " +
						"LOWER(product.product_name) = LOWER('"+p.getName()+"') AND "+
						"LOWER(category.category_name) = LOWER('"+p.getCategory().getName()+"') AND " +
						"LOWER(brand.brand_name) = LOWER('"+p.getBrand().getName()+"')";
				
				if(checkDb(query, "product_category,category,product,brand") == -1) {
					query = idCategory+","+idProduct+","+idBrand;
					connection.Insert("product_category", query);
				}
			}
		}
		
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println(Colors.ANSI_GREEN+"INFO:"+storedProduct+" products stored and "+updatedProduct+" products updated!"+Colors.ANSI_RESET);
	}
	
	// Verify if a element exist on a database
	public int checkDb(String query, String tables) {
		int id = -1;
		
		ResultSet rs = connection.StartQuery(query, tables);
		
		try {
			if(rs.next()) id = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	// Recover the last id in the database
	private int getLastID(String table, String key) {
		ResultSet rs = connection.QueryAll("SELECT * FROM "+table+" ORDER BY "+key+" ASC");
		int lastId = -1;
		
		// Get the last id in the database
		try {
			if(rs.next()) {
				rs.last();
				lastId = rs.getInt(key);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lastId;
	}
}
