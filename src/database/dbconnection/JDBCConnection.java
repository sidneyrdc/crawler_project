package database.dbconnection;

import java.sql.*;

public class JDBCConnection {
	private String sql_str;
	private Connection db;
	private Statement sq_stmt;
	private ResultSet rs;
	
	public void StartConnection(String login, String password, String hostname, String database, String port) throws SQLException {
		String url = "jdbc:postgresql://"+hostname+":"+port+"/"+database;
		
		db = DriverManager.getConnection(url, login, password);
		sq_stmt = db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	}
	
	public void CloseConnection() throws SQLException{
		sq_stmt.close();
		rs.close();
		db.close();
	}
	
	public ResultSet StartQuery(String condicao, String table) {
		sql_str = "SELECT * FROM " + table + " WHERE " + condicao;
		try {
			rs = sq_stmt.executeQuery(sql_str);
		} catch (SQLException e) {
//			e.printStackTrace();
		}
		return rs;
	}
	
	public void Insert(String table, String value) {
		sql_str = "INSERT INTO "+ table +" VALUES ("+ value +");";
		try {
			rs = sq_stmt.executeQuery(sql_str);
		} catch (SQLException e) {
//			e.printStackTrace();
//			System.out.println(e.getMessage());
//			if(!e.getMessage().contains("No results were returned by the query")) {
//				System.exit(1);
//			}
		}
	}
	
	public ResultSet QueryAll(String query){
		try {
			rs = sq_stmt.executeQuery(query);
		} catch (SQLException e) {
//			e.printStackTrace();
		}
		
		return rs;
	}
	
	public void Update(String table, String value, String condition) {
		sql_str = "UPDATE "+ table +" SET "+ value +" WHERE "+ condition;
		
		try {
			rs = sq_stmt.executeQuery(sql_str);
		} catch (SQLException e) {
//			e.printStackTrace();
		}
	}
	
	public void Remove(String table, String value){
		sql_str = "DELETE FROM "+ table +" WHERE "+ value;
		try {
			rs = sq_stmt.executeQuery(sql_str);
		} catch (SQLException e) {
//			e.printStackTrace();
		}
	}
	
	public int tablesNumber() {
		sql_str = "SELECT COUNT(*) FROM pg_tables WHERE schemaname='public'";
		
		try {
			rs = sq_stmt.executeQuery(sql_str);
			rs.next();
			return rs.getInt("count");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public void createDB() {
		sql_str = "CREATE TABLE public.shop ("+
                  "shop_id INTEGER NOT NULL,"+
                  "shop_name VARCHAR,"+
                  "shop_description VARCHAR,"+
                  "shop_url VARCHAR,"+
                  "CONSTRAINT shop_pk PRIMARY KEY (shop_id));"+
                  "CREATE TABLE public.brand ("+
                  "brand_id INTEGER NOT NULL,"+
                  "brand_name VARCHAR,"+
                  "brand_description VARCHAR,"+
                  "brand_logo VARCHAR,"+
                  "CONSTRAINT brand_pk PRIMARY KEY (brand_id));"+
                  "CREATE TABLE public.category ("+
                  "category_id INTEGER NOT NULL,"+
                  "category_name VARCHAR,"+
                  "category_description VARCHAR,"+
                  "category_seeds VARCHAR,"+
                  "CONSTRAINT category_pk PRIMARY KEY (category_id));"+
                  "CREATE TABLE public.product ("+
                  "product_id INTEGER NOT NULL,"+
                  "brand_id INTEGER NOT NULL,"+
                  "product_name VARCHAR,"+
                  "product_description VARCHAR,"+
                  "product_img_url VARCHAR,"+
                  "product_hqimg_url VARCHAR,"+
                  "CONSTRAINT product_pk PRIMARY KEY (product_id, brand_id));"+
                  "CREATE TABLE public.product_shop ("+
                  "shop_id INTEGER NOT NULL,"+
                  "product_id INTEGER NOT NULL,"+
                  "brand_id INTEGER NOT NULL,"+
                  "product_shop_price REAL,"+
                  "product_shop_discount REAL,"+
                  "product_shop_url VARCHAR,"+
                  "product_shop_outofstock BOOLEAN DEFAULT False,"+
                  "product_shop_rate NUMERIC,"+
                  "CONSTRAINT product_shop_pk PRIMARY KEY (shop_id, product_id, brand_id));"+
                  "CREATE TABLE public.product_category ("+
                  "category_id INTEGER NOT NULL,"+
                  "product_id INTEGER NOT NULL,"+
                  "brand_id INTEGER NOT NULL,"+
                  "CONSTRAINT product_category_pk PRIMARY KEY (category_id, product_id, brand_id));"+
                  "ALTER TABLE public.product_shop ADD CONSTRAINT shop_product_shop_fk "+
                  "FOREIGN KEY (shop_id) "+
                  "REFERENCES public.shop (shop_id) "+
                  "ON DELETE NO ACTION "+
                  "ON UPDATE NO ACTION "+
                  "NOT DEFERRABLE;"+
                  "ALTER TABLE public.product ADD CONSTRAINT brand_product_fk "+
                  "FOREIGN KEY (brand_id) "+
                  "REFERENCES public.brand (brand_id) "+
                  "ON DELETE NO ACTION "+
                  "ON UPDATE NO ACTION "+
                  "NOT DEFERRABLE;"+
                  "ALTER TABLE public.product_category ADD CONSTRAINT category_product_category_fk "+
                  "FOREIGN KEY (category_id) "+
                  "REFERENCES public.category (category_id) "+
                  "ON DELETE NO ACTION "+
                  "ON UPDATE NO ACTION "+
                  "NOT DEFERRABLE;"+
                  "ALTER TABLE public.product_category ADD CONSTRAINT product_product_category_fk "+
                  "FOREIGN KEY (product_id, brand_id) "+
                  "REFERENCES public.product (product_id, brand_id) "+
                  "ON DELETE NO ACTION "+
                  "ON UPDATE NO ACTION "+
                  "NOT DEFERRABLE;"+
                  "ALTER TABLE public.product_shop ADD CONSTRAINT product_product_shop_fk "+
                  "FOREIGN KEY (product_id, brand_id) "+
                  "REFERENCES public.product (product_id, brand_id) "+
                  "ON DELETE NO ACTION "+
                  "ON UPDATE NO ACTION "+
                  "NOT DEFERRABLE;";
		
		try {
			sq_stmt.executeQuery(sql_str);
		} catch (SQLException e) {
//			e.printStackTrace();
		}
		
		sql_str = "INSERT INTO category VALUES (0,'Make-up','','http://www.sephora.com.br/maquiagem/acessorios\n"+
				  "http://www.sephora.com.br/maquiagem/corpo\n"+
				  "http://www.sephora.com.br/maquiagem/face\n"+
				  "http://www.sephora.com.br/maquiagem/kits-de-maquiagem\n"+
				  "http://www.sephora.com.br/maquiagem/olhos\n"+
				  "http://www.sephora.com.br/maquiagem/labios\n"+
				  "http://www.belezanaweb.com.br/maquiagem/kit-conjunto-paleta/\n"+
				  "http://www.belezanaweb.com.br/maquiagem/rosto/\n"+
				  "http://www.belezanaweb.com.br/maquiagem/olhos/\n"+
				  "http://www.belezanaweb.com.br/maquiagem/labios/\n"+
				  "http://www.belezanaweb.com.br/maquiagem/corpo/\n"+
				  "http://www.belezanaweb.com.br/maquiagem/pinceis/\n"+
				  "http://www.belezanaweb.com.br/maquiagem/acessorios/\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/labios/batom\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/face/base\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/face/po-facial\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/olhos/sombra\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/olhos/mascara\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/primer-e-finalizador/fixador-da-maquiagem\n"+
				  "http://www.epocacosmeticos.com.br/maquiagem/aplicadores-para-maquiagem/pincel-ou-aplicador');"+
				  "INSERT INTO category VALUES (1,'Nails','','http://www.sephora.com.br/maquiagem/unhas\n"+
				  "http://www.sephora.com.br/tratamento/unhas/\n"+
				  "http://www.belezanaweb.com.br/unhas/kits/\n"+
				  "http://www.belezanaweb.com.br/unhas/esmaltes/\n"+
				  "http://www.belezanaweb.com.br/unhas/bases/\n"+
				  "http://www.belezanaweb.com.br/unhas/tratamentos/\n"+
				  "http://www.belezanaweb.com.br/unhas/finalizadores/\n"+
				  "http://www.epocacosmeticos.com.br/tratamentos/unhas/');"+
				  "INSERT INTO category VALUES (2,'Skincare','','http://www.sephora.com.br/tratamento/corpo\n"+
				  "http://www.sephora.com.br/tratamento/face\n"+
				  "http://www.sephora.com.br/tratamento/kits-de-tratamento\n"+
				  "http://www.sephora.com.br/tratamento/labios\n"+
				  "http://www.sephora.com.br/tratamento/maos\n"+
				  "http://www.sephora.com.br/tratamento/olhos\n"+
				  "http://www.sephora.com.br/tratamento/pes\n"+
				  "http://www.sephora.com.br/tratamento/pescoco\n"+
				  "http://www.sephora.com.br/tratamento/seios\n"+
				  "http://www.sephora.com.br/corpo-e-banho/autobronzeador\n"+
				  "http://www.sephora.com.br/corpo-e-banho/banho\n"+
				  "http://www.sephora.com.br/corpo-e-banho/bronzeador\n"+
				  "http://www.sephora.com.br/corpo-e-banho/desodorante\n"+
				  "http://www.sephora.com.br/corpo-e-banho/esfoliante\n"+
				  "http://www.sephora.com.br/corpo-e-banho/kits-de-corpo-e-banho\n"+
				  "http://www.sephora.com.br/corpo-e-banho/locao-corporal\n"+
				  "http://www.sephora.com.br/corpo-e-banho/oleo\n"+
				  "http://www.sephora.com.br/corpo-e-banho/pes\n"+
				  "http://www.sephora.com.br/corpo-e-banho/protetor-solar\n"+
				  "http://www.sephora.com.br/corpo-e-banho/sabonete\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/kits-de-tratamentos/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/tipo-de-pele/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/rosto/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/olhos/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/labios/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/maos/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/pes/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/corpo/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/seios/\n"+
				  "http://www.belezanaweb.com.br/cuidados-para-pele/mamae-e-bebe/\n"+
				  "http://www.belezanaweb.com.br/corpo-e-banho/sabonete/\n"+
				  "http://www.belezanaweb.com.br/corpo-e-banho/desodorante/\n"+
				  "http://www.belezanaweb.com.br/corpo-e-banho/hidratante/\n"+
				  "http://www.belezanaweb.com.br/corpo-e-banho/oleo/\n"+
				  "http://www.belezanaweb.com.br/corpo-e-banho/autobronzeador-e-bronzeador/\n"+
				  "http://www.belezanaweb.com.br/corpo-e-banho/protetor-solar/\n"+
				  "http://www.epocacosmeticos.com.br/dermocosmeticos/cuidados-com-o-sol/protetor-solar\n"+
				  "http://www.epocacosmeticos.com.br/dermocosmeticos/face/cuidados-faciais-especificos\n"+
				  "http://www.epocacosmeticos.com.br/dermocosmeticos/face/limpadores\n"+
				  "http://www.epocacosmeticos.com.br/dermocosmeticos/nutricosmeticos\n"+
				  "http://www.epocacosmeticos.com.br/dermocosmeticos/face/hidratantes\n"+
				  "http://www.epocacosmeticos.com.br/tratamentos/face/cuidados-faciais-especificos\n"+
				  "http://www.epocacosmeticos.com.br/tratamentos/face/hidratantes-faciais\n"+
				  "http://www.epocacosmeticos.com.br/corpo-e-banho/banho/sabonete-e-gel-de-banho\n"+
				  "http://www.epocacosmeticos.com.br/corpo-e-banho/pos-banho/hidratante\n"+
				  "http://www.epocacosmeticos.com.br/corpo-e-banho/pos-banho/hidratante\n"+
				  "http://www.epocacosmeticos.com.br/corpo-e-banho/pos-banho/oleo');"+
				  "INSERT INTO category VALUES (3,'Hair','','http://www.sephora.com.br/cabelos/acessorios\n"+
				  "http://www.sephora.com.br/cabelos/condicionador\n"+
				  "http://www.sephora.com.br/cabelos/finalizador\n"+
				  "http://www.sephora.com.br/cabelos/kits-para-cabelos\n"+
				  "http://www.sephora.com.br/cabelos/mascara\n"+
				  "http://www.sephora.com.br/cabelos/modelador\n"+
				  "http://www.sephora.com.br/cabelos/secadores-e-modeladores\n"+
				  "http://www.sephora.com.br/cabelos/shampoo\n"+
				  "http://www.sephora.com.br/cabelos/tratamentos-especificos\n"+
				  "http://www.belezanaweb.com.br/cabelos/tratamentos-completos/\n"+
				  "http://www.belezanaweb.com.br/cabelos/shampoo/\n"+
				  "http://www.belezanaweb.com.br/cabelos/condicionador/\n"+
				  "http://www.belezanaweb.com.br/cabelos/tratamentos/\n"+
				  "http://www.belezanaweb.com.br/cabelos/finalizadores/\n"+
				  "http://www.belezanaweb.com.br/termicos-e-acessorios/\n"+
				  "http://www.belezanaweb.com.br/cabelos/acessorios-para-cabelos/\n"+
				  "http://www.belezanaweb.com.br/cabelos/tipos-de-cabelo/\n"+
				  "http://www.epocacosmeticos.com.br/cabelos/shampoo\n"+
				  "http://www.epocacosmeticos.com.br/cabelos/condicionador\n"+
				  "http://www.epocacosmeticos.com.br/cabelos/mascara\n"+
				  "http://www.epocacosmeticos.com.br/cabelos/finalizador\n"+
				  "http://www.epocacosmeticos.com.br/cabelos/tratamento\n"+
				  "http://www.epocacosmeticos.com.br/cabelos/coloracao/tintura-para-cabelos');"+
				  "INSERT INTO category VALUES (4,'Fragrances','','http://www.sephora.com.br/perfumes/feminino\n"+
				  "http://www.sephora.com.br/perfumes/kits-de-perfumes-para-ela\n"+
				  "http://www.belezanaweb.com.br/perfumes/femininos/\n"+
				  "http://www.epocacosmeticos.com.br/perfumes/perfume-feminino');";
		
		try {
			sq_stmt.executeQuery(sql_str);
		} catch (SQLException e) {
//			e.printStackTrace();
		}
	}
}
