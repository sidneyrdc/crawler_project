package crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import structs.Brand;
import structs.Category;
import structs.Product;
import structs.Shop;
import utils.Colors;

public class Crawler {
	// Extract products to a category
	public ArrayList<Product> extractProducts(String[] baseLinks, String category) {
		ArrayList<Product> products = new ArrayList<>(); 
		
		// Select the correct crawler to baselink
		for(String url : baseLinks) {
			if(url.contains("sephora")) products.addAll(sephoraProducts(url, category));
			if(url.contains("belezanaweb")) products.addAll(belezanawebProducts(url, category));
			if(url.contains("epocacosmeticos")) products.addAll(epocacosmeticosProducts(url, category));
		}
		
		return products;
	}
	
	// Extract url from html text
	private String extractUrl(String html) {
		String[] imgs = html.split("src=\"");

		if(imgs.length <= 1) return null;
		
		imgs = imgs[1].split("\"");
		
		String patternString = "http.*";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(imgs[0]);
        
        if(matcher.find()) return imgs[0];
        
        return null;
	}
	
	// Extract text between two delimiters
	private String extractTerm(String input, String del1, String del2) {
		String[] textBlocks = input.split(del1);
		
		if(textBlocks.length <= 1) return "";
		
		textBlocks = textBlocks[1].split(del2);
		
		return textBlocks[0];
	}
	
	// Remove illegal characters from input string
	private String fixStr(String input) {
		
		if(input != null) {
			input = input.replaceAll("\'", "\'\'");
			input =	input.replaceAll("", "\'\'");
			input = input.replaceAll("", "");
			input = input.replaceAll("", "\"");
			input = input.replaceAll("", "\"");
		}
		
		return input;
	}
	
	// Verify if a product name exist in a arraylist
	private boolean checkProduct(ArrayList<Product> productArray, String nameKey) {
		for(Product p : productArray) {
			if(p.getName().equalsIgnoreCase(nameKey)) return true;
		}
		
		return false;
	}
	
	// Extract products from epocacosmeticos (http://www.epocacosmeticos.com.br/)
	private ArrayList<Product> epocacosmeticosProducts(String baseLink, String category) {
		ArrayList<Product> output = new ArrayList<>();
		Document doc;
		String[] page_keys = {"&O=OrderByNameASC","&O=OrderByNameDESC","&O=OrderByPriceASC",
							  "&O=OrderByPriceDESC","&O=OrderByTopSaleDESC",
							  "&O=OrderByReviewRateDESC","&O=OrderByReleaseDateDESC"};

		try {
			for(String key : page_keys) {
				// Connect to base link of first page
				doc = Jsoup.connect(baseLink+"?PS=80"+key).timeout(10000).get();
				
				System.out.println("-------------------------------------------------------------------------------");
				System.out.println(Colors.ANSI_RED+"INFO:Extracting from: "+baseLink+"?PS=80"+key+Colors.ANSI_RESET);
				
				// Select products from base link
				Elements products = doc.getElementsByAttributeValueContaining("class", "-epoca-cosmeticos");
				
				// Find all products information
				for(Element e : products) {
					String name = fixStr(e.select("h3").text());
					
					// Check if the product is in product array or is out of stock
					if(checkProduct(output, name) == false && e.getElementsByClass("outOfStock").size() == 0) {
						Product p = new Product();
						
						System.out.println("INFO:Extracting product: "+Colors.ANSI_CYAN+name+Colors.ANSI_RESET);

						p.setCategory(new Category(category, ""));
						p.setShop(new Shop("Epoca Cosmeticos", "", "http://www.epocacosmeticos.com.br"));
						p.setName(name);
						p.setPrice(Float.parseFloat(e.getElementsByClass("newPrice").text().split("R\\$")[1].trim().replaceAll("\\.", "").replaceAll(",", "\\.")));
						p.setImg_url(fixStr(extractUrl(e.getElementsByClass("productImage").toString())));
						p.setUrl(fixStr(extractTerm(e.getElementsByClass("comprar").toString(), "href=\"", "\"")));
						
						// Temporary connection to extract product description, high quality image and brand logo 
						Document tmp_doc = Jsoup.connect(p.getUrl()).timeout(10000).get();

						// Get product discount
						Elements prod_disc = tmp_doc.getElementsByClass("skuListPrice");

						// Verify if exist discounted price
						if(prod_disc.size() > 0) {
							// Process product discount
							float first_price = Float.parseFloat(prod_disc.get(0).text().split("R\\$")[1].trim().replaceAll("\\.", "").replaceAll(",", "\\."));

							// Set product discount
							if(first_price > 0) p.setDiscount(Math.round(100-(100*p.getPrice()/first_price)));
						}

						// Get product description
						Elements prod_desc = tmp_doc.getElementsByClass("productDescription");

						// Set product description
						p.setDescription(fixStr(prod_desc.text()));
						
						// Get high quality image url
						Elements hqimg = tmp_doc.getElementsByAttributeValueContaining("id", "image-main");
						
						// Set high quality image
						p.setHqimg_url(fixStr(extractUrl(hqimg.toString())));
						
						// Get brand logo
						Elements brand_logo = tmp_doc.getElementsByClass("name_brand");
						String[] tmp = extractTerm(brand_logo.toString(), "href=\"", "\"").split("/");
						
						if(tmp.length > 0) {						
							Document brand_doc = Jsoup.connect(p.getShop().getUrl()+"/"+tmp[tmp.length-1]).timeout(10000).get();
							String logo = fixStr(extractTerm(brand_doc.getElementsByClass("desc").toString(), "src=\"", "\""));
							
							// Set brand
							if(logo != null && !logo.equals("")) {
								p.setBrand(new Brand(fixStr(e.getElementsByClass("brand").get(0).text()), "", p.getShop().getUrl()+logo));
							} else {
								p.setBrand(new Brand(fixStr(e.getElementsByClass("brand").get(0).text()), "", ""));
							}
						} else {
							p.setBrand(new Brand(fixStr(e.getElementsByClass("brand").get(0).text()), "", ""));
						}
						
						// Add product to product list
						output.add(p);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return output;
	}
	
	// Extract products from belezanaweb (http://www.belezanaweb.com.br)
	private ArrayList<Product> belezanawebProducts(String baseLink, String category) {
		ArrayList<Product> output = new ArrayList<>();
		int actual_page = 1;
		int act_prod = -1;
		int n_prods = 0;
		int np = 1; 
		Document doc;
		Elements pages;
		
		try {
			while(act_prod < n_prods) {
				// Connect to base link of first page
				doc = Jsoup.connect(baseLink+"?ordem=p.nome|ASC&pg="+actual_page)
					       .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
					       .timeout(10000).get();

				// Get the number of pages to the base link
				pages = doc.getElementsByClass("contagem_pages");

				// Get the amount of products in this page
				act_prod = Integer.parseInt(pages.get(1).getElementsByClass("contagem").text().split("-")[1].trim());

				// Get the total of products in this url
				n_prods = Integer.parseInt(pages.get(1).getElementsByClass("itens_totais").text().trim());
				
				System.out.println("-------------------------------------------------------------------------------");
				System.out.println(Colors.ANSI_RED+"INFO:Extracting "+act_prod+" of "+n_prods+" product(s) from: "+baseLink+Colors.ANSI_RESET);
			
				// Select links from <item> tag (belezanaweb only)
				Elements products = doc.getElementsByClass("item");
				
				// Find all products information
				for(Element e : products) {
					Product p = new Product();
					
					p.setCategory(new Category(category, ""));
					p.setShop(new Shop("Beleza na Web", "", "http://www.belezanaweb.com.br"));
					p.setName(fixStr(e.getElementsByClass("nome_produto").get(0).text()));
					p.setPrice(Float.parseFloat(e.getElementsByClass("preco_promocao").get(0).text().split("R\\$")[1].trim().replaceAll("\\.", "").replaceAll(",", "\\.")));
					p.setDiscount(Float.parseFloat(0+e.getElementsByClass("porcetagem").text().split("%")[0].trim()));
					p.setImg_url(fixStr(extractUrl(e.getElementsByClass("img_produto").toString())));
					p.setUrl(fixStr(extractTerm(e.getElementsByClass("nome_produto").toString(), "href=\"", "\"")));
					
					System.out.println("INFO:Extracting product "+Colors.ANSI_RED+(np++)+Colors.ANSI_RESET+" of "+Colors.ANSI_RED+act_prod+Colors.ANSI_RESET+": "+Colors.ANSI_CYAN+p.getName()+Colors.ANSI_RESET);
					
					// Temporary connection to extract product description, high quality image and brand logo 
					Document tmp_doc = Jsoup.connect(p.getUrl())
											.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
											.timeout(10000).get();
					
					// Get product description
					Elements prod_desc = tmp_doc.getElementsByAttributeValueContaining("id","descricao_conteudo");
					
					// Set product description
					p.setDescription(fixStr(prod_desc.text()));
					
					// Get high quality image url
					Elements hqimg = tmp_doc.getElementsByClass("pics");
					
					// Set high quality image url
					p.setHqimg_url(fixStr(extractUrl(hqimg.select("img").toString())));
					
					// Get brand logo
					Elements brand_logo = tmp_doc.getElementsByClass("marca_produto");
					
					// Set brand
					p.setBrand(new Brand(fixStr(e.getElementsByClass("marca").text().split("\\+")[0].trim()), "", fixStr(extractUrl(brand_logo.toString()))));
					
					// Add product to product list
					output.add(p);
				}
				
				// Increment the page number
				actual_page++;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return output;
	}
	
	// Extract products from sephora (http://www.sephora.com.br)
	private ArrayList<Product> sephoraProducts(String baseLink, String category) {
		ArrayList<Product> output = new ArrayList<>();
		int n_pages = 1;
		
		try {
			// Connect to base link of first page
			Document doc = Jsoup.connect(baseLink+"?orderby=az&page=1").timeout(10000).get();
			
			// Get the number of pages to the base link
			Elements pages = doc.getElementsByClass("resultadoBuscaConteudoListaPaginasUltima");
			
			// Verify if the link has only one page
			if(!pages.isEmpty()) n_pages = Integer.parseInt(extractTerm(pages.get(0).toString(), "page=", "\""));
			
			// Get all pages of products
			for(int i = 1; i <= n_pages; i++) {
				int np = 1; 
				
				System.out.println("-------------------------------------------------------------------------------");
				System.out.println(Colors.ANSI_RED+"INFO:Extracting "+i+" of "+n_pages+" page(s) from: "+baseLink+Colors.ANSI_RESET);
				
				// Select links from <produto> tag (sephora only)
				Elements products = doc.getElementsByClass("produto");
				
				// Find all product information
				for(Element e : products) {
					Product p = new Product();
					
					p.setCategory(new Category(category, ""));
					p.setShop(new Shop("Sephora", "", "http://www.sephora.com.br"));
					p.setName(fixStr(e.getElementsByClass("tituloProduto").text()));
					p.setPrice(Float.parseFloat(e.getElementsByClass("produtoPrecoVendaPor").text().split("R\\$")[1].trim().replaceAll("\\.", "").replaceAll(",", "\\.")));
					p.setDiscount(Float.parseFloat(0+extractTerm(e.getElementsByAttributeValueContaining("class", "Desconto Desconto").toString(), "<div class=\"Desconto Desconto", "\"></div>")));
					p.setImg_url(fixStr(extractUrl(e.getElementsByClass("imagemProduto").toString())));
					p.setUrl(fixStr(p.getShop().getUrl()+extractTerm(e.getElementsByClass("tituloProduto").toString(), "href=\"", "\"")));
					
					System.out.println("INFO:Extracting "+Colors.ANSI_RED+(np++)+Colors.ANSI_RESET+" of "+Colors.ANSI_RED+products.select("li").size()+Colors.ANSI_RESET+" product(s): "+Colors.ANSI_CYAN+p.getName()+Colors.ANSI_RESET);
					
					// Temporary connection to extract product description, high quality image and brand logo
					Document tmp_doc = Jsoup.connect(p.getUrl()).timeout(10000).get();
					
					// Get product description
					Elements prod_desc = tmp_doc.getElementsByAttributeValue("class", "descricaoProduto");
					Elements prod_desc2 = tmp_doc.getElementsByAttributeValue("id", "fulldescription");
					
					// Set product description
					if(prod_desc2.size() > 0) {
						p.setDescription(fixStr(prod_desc2.select("p").text().replaceAll("fechar", "")));
					} else {
						p.setDescription(fixStr(prod_desc.select("p").text().replaceAll("fechar", "")));
					}
					
					// Get high quality image link
					Elements hqimg = tmp_doc.getElementsByAttributeValueContaining("class", "lateralImagemProduto");
					
					// Set the high quality image link
					if(hqimg.size() > 0 && hqimg.select("img").size() > 0) {
						p.setHqimg_url(fixStr(extractUrl(hqimg.select("img").get(0).toString())));
					}
					
					// Get brand logo
					Elements brand_logo = tmp_doc.getElementsByClass("tituloDescricaoProduto");
					
					// Set brand
					p.setBrand(new Brand(fixStr(e.getElementsByClass("produtoMarca").text()), "", fixStr(extractTerm(brand_logo.toString(), "src=\"", "\""))));
					
					// Add product to product list
					output.add(p);
				}
				
				// Connect to base link of next page 
				doc = Jsoup.connect(baseLink+"?orderby=az&page="+(i+1)).timeout(10000).get();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return output;
	}
}
