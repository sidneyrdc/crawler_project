package crawler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import database.access.DatabaseManager;
import structs.Category;
import structs.Product;
import utils.Colors;

public class Main {
	public static void main(String[] args) throws IOException {
		ArrayList<Product> products = new ArrayList<>(); 
		Crawler crawler = new Crawler();
		
		System.out.println("###############################################################################");
		System.out.println(Colors.ANSI_GREEN+"INFO:Extracting data..."+Colors.ANSI_RESET);
		
		long startTime = System.currentTimeMillis();
		
		// Extract product to make-up category
		String[] makeup = {"http://www.belezanaweb.com.br/maquiagem/rosto/",
				/*"http://www.sephora.com.br/maquiagem/corpo",
				"http://www.sephora.com.br/maquiagem/face",
				"http://www.sephora.com.br/maquiagem/kits-de-maquiagem",
				"http://www.sephora.com.br/maquiagem/olhos",
				"http://www.sephora.com.br/maquiagem/labios",
				"http://www.belezanaweb.com.br/maquiagem/kit-conjunto-paleta/",
				"http://www.belezanaweb.com.br/maquiagem/rosto/",
				"http://www.belezanaweb.com.br/maquiagem/olhos/",
				"http://www.belezanaweb.com.br/maquiagem/labios/",
				"http://www.belezanaweb.com.br/maquiagem/corpo/",
				"http://www.belezanaweb.com.br/maquiagem/pinceis/",
				"http://www.belezanaweb.com.br/maquiagem/acessorios/",
				"http://www.epocacosmeticos.com.br/maquiagem/labios/batom",
				"http://www.epocacosmeticos.com.br/maquiagem/face/base",
				"http://www.epocacosmeticos.com.br/maquiagem/face/po-facial",
				"http://www.epocacosmeticos.com.br/maquiagem/olhos/sombra",
				"http://www.epocacosmeticos.com.br/maquiagem/olhos/mascara",
				"http://www.epocacosmeticos.com.br/maquiagem/primer-e-finalizador/fixador-da-maquiagem",
				"http://www.epocacosmeticos.com.br/maquiagem/aplicadores-para-maquiagem/pincel-ou-aplicador",
				"http://www.epocacosmeticos.com.br/tratamentos/unhas"*/};
		
		products.addAll(crawler.extractProducts(makeup, "Make-up"));
		
		String[] nails = {/*"http://www.sephora.com.br/maquiagem/unhas",
				"http://www.sephora.com.br/tratamento/unhas",
				"http://www.belezanaweb.com.br/unhas/kits/",
				"http://www.belezanaweb.com.br/unhas/esmaltes/",
				"http://www.belezanaweb.com.br/unhas/bases/",
				"http://www.belezanaweb.com.br/unhas/tratamentos/",
				"http://www.belezanaweb.com.br/unhas/finalizadores/",
				"http://www.epocacosmeticos.com.br/tratamentos/unhas"*/};
		
//		products.addAll(crawler.extractProducts(nails, "nails"));

		
		
		System.out.println("###############################################################################");
		System.out.println(Colors.ANSI_GREEN+"INFO:Storing data..."+Colors.ANSI_RESET);
		System.out.println("-------------------------------------------------------------------------------");
		
//		Product p = new Product();
//		
//		p.setName("adljfa'asjflas");
//		
//		products.add(p);
		
		DatabaseManager dbstore = new DatabaseManager("postgres", "localhost", "postgres", "123456", "5432");
		dbstore.storeProducts(products);
		
//		Category c = new Category("blabla", "");
//		Category c2 = new Category("animal", "today is art");
//		
//		c2.setSeeds("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
//		
//		dbstore.storeSeeds(c,c2);
		
		long endTime = System.currentTimeMillis();
		
		System.out.println("###############################################################################");
		System.out.println(Colors.ANSI_GREEN+"INFO:"+products.size()+" products extracted in "+(endTime - startTime)+" ms"+Colors.ANSI_RESET);
		
//		for(Product p : products) {
//			writeFile("\n"+p.toString(), "data");
//		}
	}
	
	// write data in a file
	public static void writeFile(String data, String fileName) {
		File file = new File(fileName);

		PrintWriter output;
		try {
			output = new PrintWriter(new FileWriter(file, true));
			output.write(data);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
